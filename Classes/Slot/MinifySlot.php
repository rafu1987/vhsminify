<?php
declare (strict_types = 1);
namespace RZ\Vhsminify\Slot;

/*
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */

/**
 * Minify vhs assets
 *
 * @author Raphael Zschorsch <rafu1987@gmail.com>
 */
class MinifySlot
{

    /**
     * @param string $file
     * @param string $contents
     *
     * @return string
     */
    public function minifyAssets(&$file, &$contents)
    {
        $type = pathinfo($file, PATHINFO_EXTENSION);
        $type = strtoupper($type);

        if (($GLOBALS['TSFE']->tmpl->setup['plugin.']['tx_vhs.']['assets.']['compressCss'] == 1 && $type === 'CSS')
            || ($GLOBALS['TSFE']->tmpl->setup['plugin.']['tx_vhs.']['assets.']['compressJs'] == 1 && $type === 'JS')) {

            $minifierClassName = '\\MatthiasMullie\\Minify\\' . $type;
            $minifier = new $minifierClassName();

            // CSS
            if ($type === 'CSS') {
                $minifier->setImportExtensions([]);
                $contents = $this->compressCssString($contents);
            }

            $minifier->add($contents);
            $contents = $minifier->minify();
        }
    }

    /**
     * Compress a CSS string by removing comments and whitespace characters
     *
     * @param string $contents
     * @return string
     */
    protected function compressCssString($contents)
    {
        // Perform some safe CSS optimizations.
        // Regexp to match comment blocks.
        $comment = '/\*[^*]*\*+(?:[^/*][^*]*\*+)*/';
        // Regexp to match double quoted strings.
        $double_quot = '"[^"\\\\]*(?:\\\\.[^"\\\\]*)*"';
        // Regexp to match single quoted strings.
        $single_quot = "'[^'\\\\]*(?:\\\\.[^'\\\\]*)*'";
        // Strip all comment blocks, but keep double/single quoted strings.
        $contents = preg_replace(
            "<($double_quot|$single_quot)|$comment>Ss",
            '$1',
            $contents
        );
        // Remove certain whitespace.
        // There are different conditions for removing leading and trailing
        // whitespace.
        // @see http://php.net/manual/regexp.reference.subpatterns.php
        $contents = preg_replace(
            '<
                # Strip leading and trailing whitespace.
                \s*([@{};,])\s*
                # Strip only leading whitespace from:
                # - Closing parenthesis: Retain "@media (bar) and foo".
                | \s+([\)])
                # Strip only trailing whitespace from:
                # - Opening parenthesis: Retain "@media (bar) and foo".
                # - Colon: Retain :pseudo-selectors.
                | ([\(:])\s+
                >xS',
            // Only one of the three capturing groups will match, so its reference
            // will contain the wanted value and the references for the
            // two non-matching groups will be replaced with empty strings.
            '$1$2$3',
            $contents
        );
        // End the file with a new line.
        $contents = trim($contents);
        // Ensure file ends in newline.
        $contents .= LF;
        return $contents;
    }

}