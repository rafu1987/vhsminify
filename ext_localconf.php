<?php

/*
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */

defined('TYPO3_MODE') || die();

$boot = function () {
    if (class_exists('FluidTYPO3\Vhs\Service\AssetService')) {
        /** @var \TYPO3\CMS\Extbase\SignalSlot\Dispatcher $signalSlotDispatcher */
        $signalSlotDispatcher = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\\CMS\\Extbase\\SignalSlot\Dispatcher');

        $signalSlotDispatcher->connect(
            \FluidTYPO3\Vhs\Service\AssetService::class,
            \FluidTYPO3\Vhs\Service\AssetService::ASSET_SIGNAL,
            \RZ\Vhsminify\Slot\MinifySlot::class,
            'minifyAssets',
            false
        );
    }
};

$boot();
unset($boot);